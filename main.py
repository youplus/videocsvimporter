import pandas as pd
import json
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import re
import math
import json
from bson import ObjectId
from datetime import datetime
filenames = ["lipstick"]
from util.db import DB
import copy

dBinstance = DB()
#TODO
# thumbnails and transcription , timelist , timestamps are missing

with open('record.json') as json_file:
    templateRecord = json.load(json_file)
#filenames = ["Brand"]
order_number=1
for filename in filenames:
    xls = pd.ExcelFile(filename+".xlsx")

    study_id=ObjectId("5e16f31c59af31472dd59d9a")
    country ="US"
    product_type="study_country_question"

    for sheet in xls.sheet_names :
          
        
        title = filename + " study"
        question="What is your view on"+ " "+filename+"?"
        questionId = ObjectId()
        
        
        df = pd.read_excel (filename+".xlsx",header=None,sheet_name=sheet, skiprows=[0])
        categories = []


        rowSize = len(df.index)

        columnSize = int(int(str(df.size))/rowSize)

        print("column size:", columnSize)
        print("rowSize:" ,rowSize)
        seriesValue=[]


        data=[]

        for i in range (0,rowSize-1):
            record =copy.deepcopy(templateRecord)
            record["_id"]=ObjectId()
            record["question"] = question
            record["question_id"] = questionId
            record["question_order_number"]=1
            record["study_id"]=study_id
            record["title"]=title
            record["product_type"]=product_type
            record["upload_date"] = datetime.now()
            if df[0][i] is not "":
                try:
                    record["industry_code"]=1
                    
                    
                    record["video_url"] = df[0][i]
                    record["transcribed_text"] = df[1][i]
                    record["attribute_list"] = eval(df[2][i])
                    record["sure_attributes_raw"] = eval(df[3][i])
                    record["sentiment_score"] = float(df[4][i])
                    record["opi_phrases"] = eval(df[5][i])
                    record["describers_nouns_phrases"]  = eval(df[6][i])
                    record["suggestions"] = eval(df[7][i])
                    record["describers_nouns"] = eval(df[8][i])


                    
                    
                    sure_attributes_list=[]
                    for key,value in record["sure_attributes_raw"].items():
                        
                        attr = dict()
                        attr["attribute"]=key
                        attr["intensity"] = []
                        attr["sentiment"]=0
                        for intensityAttr in value:
                            subAttr ={}
                            subAttr["polarity"] = intensityAttr["label"].capitalize()
                            subAttr["sentiment"] = intensityAttr["score"]
                            subAttr["sentence"] = intensityAttr["sentence"]
                            attr["intensity"].append(subAttr)
                            attr["group_attribute"]="Others"
                            sure_attributes_list.append(attr)
                        record.pop("sure_attributes_raw",None)

                        record["sure_attributes"]=sure_attributes_list
                        print(record["sure_attributes"])
                      
                
                    
                except Exception as ex:
                    print(ex)
                dBinstance.insertVideo(record)

            
            
            




print("final order number is ", order_number)



                        




                
  
   
      
  